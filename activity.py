# ACTIVITY
name, age, occupation, movie, rating = "Jose", 38, "writer", "One more chance", 99.6

print(f"I am {name}, and I am {age} years old, I work as a {occupation} , and my rating for {movie} is {rating}%")

num1, num2, num3 = 10, 15, 20

print("Product of num1 and num2 is: " + str(num1 * num2))
print("Is num1 less than num3? " + str(num1 < num3))
print("Sum of num3 and num2: " + str(num3 + num2))